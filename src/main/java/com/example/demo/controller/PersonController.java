package com.example.demo.controller;

import com.example.demo.entity.Person;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.example.demo.service.PersonService.*;

@RestController
@RequestMapping("")
@CrossOrigin
public class PersonController {

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    Environment environment;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonService personService;

    final String htmlFile = "index.html";

    //public PersonController(PersonRepository frontendController) { this.personRepository = frontendController; }
    /*
    @PostMapping("/createMoreData")
    @ResponseStatus(code = HttpStatus.OK)
    public ModelAndView createMoreData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw,
            Model model,
            HttpServletResponse httpServletResponse
    ){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(htmlFile);
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                if(isDatabaseEmpty()) {
                    createNewData(true);
                    model.addAttribute("message",
                        "Created new data with admin account(s).");
                } else {
                    createNewData();
                    model.addAttribute("message",
                        "Created new data.");
                }
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personRepository.findAll()));
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, true);
            }
            case EMPTY_PARAMETER -> {
                log.error(PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE,
                        "Did not create new data: " + PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(false, personRepository.findAll()));
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
            }
            case WRONG_PARAMETER -> {
                log.error(PersonService.IsAdmin.WRONG_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE,
                        "Did not create new data: " + PersonService.IsAdmin.WRONG_PARAMETER.toString());
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(false, personRepository.findAll()));
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
            }
        }
        return modelAndView;
    }
     */
    /*
    @PostMapping("/findData")
    @ResponseStatus(code = HttpStatus.OK)
    public void findData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw,
            @RequestParam(required = false) String email,
            Model model
    ){
        Person person = (Person) model.getAttribute(personService.NAME_FOR_MODEL_DATA);
        if(person != null) {
            List<Person> personList = null;
            if (!(personList = personRepository.findByEmail(person.getEmail())).isEmpty()) {
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personList));
            } else if (!(personList = personRepository.findByEmailStartsWith(person.getEmail())).isEmpty()) {
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personList));
            } else if (!(personList = personRepository.findByFirstname(person.getFirstname())).isEmpty()) {
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personList));
            } else if (!(personList = personRepository.findByLastname(person.getLastname())).isEmpty()) {
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personList));
            }
        }
    }
     */
    /*
    @PostMapping("/removeData/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void removeData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw,
            @PathVariable int id,
            Model model,
            HttpServletResponse httpServletResponse){
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                Optional<Person> optionalPerson = personRepository.findById(id);
                if(optionalPerson.isPresent()) {
                    personRepository.delete(optionalPerson.get());
                } else {
                    log.error("Error: Did not find data with id {}", id);
                    model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE,
                            "Did not find person with id "+id+" -> did not remove data: " + PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                    httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
            case EMPTY_PARAMETER -> {
                log.error(PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE,
                        "Did not remove data: " + PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            case WRONG_PARAMETER -> {
                log.error(PersonService.IsAdmin.WRONG_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE,
                        "Did not remove data: " + PersonService.IsAdmin.WRONG_PARAMETER.toString());
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
    }
     */
    /*
    @GetMapping("")
    @RequestMapping
    public ModelAndView loadBoot(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw,
            Model model
    ){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(htmlFile);
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                log.info(IsAdmin.YES.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, true);
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(true, personRepository.findAll()));
            }
            case EMPTY_PARAMETER -> {
                log.error(IsAdmin.EMPTY_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE, IsAdmin.EMPTY_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse()));
            }
            case WRONG_PARAMETER -> {
                log.error(IsAdmin.WRONG_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
                model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE, IsAdmin.WRONG_PARAMETER.toString());
                model.addAttribute(personService.NAME_FOR_MODEL_DATA, getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse()));
            }
        }
        return modelAndView;
    }
     */
    @GetMapping("")
    public ModelAndView start(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(htmlFile);
        return modelAndView;
    }
    @GetMapping("/get")
    public List<Person> getData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw
    ){
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                log.info(IsAdmin.YES.toString());
                //model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, true);
                return personService.getDataWithoutSensibleInfos(true, personRepository.findAll());
            }
            case EMPTY_PARAMETER -> {
                log.error(IsAdmin.EMPTY_PARAMETER.toString());
                //model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
                //model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE, IsAdmin.EMPTY_PARAMETER.toString());
                return personService.getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse());
            }
            case WRONG_PARAMETER -> {
                log.error(IsAdmin.WRONG_PARAMETER.toString());
                //model.addAttribute(personService.NAME_FOR_MODEL_PERMISSION, false);
                //model.addAttribute(personService.NAME_FOR_MODEL_MESSAGE, IsAdmin.WRONG_PARAMETER.toString());
                return personService.getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse());
            }
        }
        return null;
    }

    @PostMapping("/createMoreData")
    @ResponseBody
    public ResponseEntity<String> createMoreData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw
            //@RequestBody(required = false) Person person
    ){
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                createNewDataAndSaceToDB();
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body("Created new data.");
            }
            case EMPTY_PARAMETER -> {
                log.error(PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body("Did not create new data: " + PersonService.IsAdmin.EMPTY_PARAMETER.toString());
            }
            case WRONG_PARAMETER -> {
                log.error(PersonService.IsAdmin.WRONG_PARAMETER.toString());
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body("Did not create new data: " + IsAdmin.WRONG_PARAMETER.toString());
            }
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("");
    }

    @PostMapping("/createNewData")
    @ResponseBody
    public ResponseEntity<String> createNewData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw
            //@RequestBody(required = false) Person person
    ){
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                emptyDBAndcreateNewData();
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body("Created new data.");
            }
            case EMPTY_PARAMETER -> {
                log.error(PersonService.IsAdmin.EMPTY_PARAMETER.toString());
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body("Did not create new data: " + PersonService.IsAdmin.EMPTY_PARAMETER.toString());
            }
            case WRONG_PARAMETER -> {
                log.error(PersonService.IsAdmin.WRONG_PARAMETER.toString());
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body("Did not create new data: " + IsAdmin.WRONG_PARAMETER.toString());
            }
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("");
    }

    /*
    @GetMapping("")
    public List<Person> loadData(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String pw
    ){
        switch (isAdminAccount(username, pw)) {
            case YES -> {
                log.info(IsAdmin.YES.toString());
                return getDataWithoutSensibleInfos(true, personRepository.findAll());
            }
            case EMPTY_PARAMETER -> {
                log.error(IsAdmin.EMPTY_PARAMETER.toString());
                return getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse());
            }
            case WRONG_PARAMETER -> {
                log.error(IsAdmin.WRONG_PARAMETER.toString());
                return getDataWithoutSensibleInfos(false, personRepository.findByIsAdminFalse());
            }
        }
        return null;
    }
     */
    /*
    @GetMapping("/email")
    @ResponseBody
    public List<Person> loadData(@RequestParam String email){
        List<Person> personList = personRepository.findAll();
        List<Person> results = null;
        if(!personList.isEmpty()){
            // find patient.id.versichertennummer -> compare with parameter
            for(Person person: personList){
                if(email.toLowerCase().equals(person.getEmail())){
                    if(results == null){
                        results = new ArrayList<>();
                    }
                    log.info("Found: person.email = " + email);
                    results.add(person);
                }
            }
        }
        return results;
    }
     */

    public void createNewDataIfNotCreated(){
        if(isDatabaseEmpty()){
            createNewDataAndSaceToDB();
        }
    }
    /*
    public void createNewData(){
        createNewData(false);
    }
    */
    /*
    public void createNewData(boolean withAdmin){
        List<Person> personList = null;
        if(withAdmin) {
            personList = createNewDataWithAdmin();
        } else {
            personList = PersonService.createNewData();
        }
        log.info("Saving all " + personList.size() + " data to database.");
        personRepository.saveAll(personList);
    }
     */
    /*
    public List<Person> createNewData(){
        List<Person> personList = PersonService.createNewData();
        //log.info("Saving " + personList.size() + " data to database.");
        //personRepository.saveAll(personList);
    }
     */

    public void emptyDBAndcreateNewData(){
        personRepository.deleteAll();
        createNewDataAndSaceToDB();
    }

    /*
    public void emptyDBAndcreateNewData(boolean withAdmin){
        personRepository.deleteAll();
        createNewData(withAdmin);
    }
     */

    public boolean isDatabaseEmpty(){
        return jdbcTemplate.queryForList("select * from " + personService.DATABASE_NAME + " limit 1;").isEmpty();
    }

    /*@Service
    public static class ViewPerson{
        String name;
        String pw;
    }
     */
    private void createNewDataAndSaceToDB(){
        List<Person> personList = PersonService.createNewData();
        log.info("Saving all "+personList.size()+" data to database.");
        personRepository.saveAll(personList);
    }
}
