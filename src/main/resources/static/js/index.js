
//@(document).ready(function (){

//const host = "http://localhost:8000";
const host = "http://derstaudamm.info:8000";
function insertDataTest()
{
  let table = $('#table');
  table.append('<tr id="tableinput">'
  +'<td>'
  +'<input placeholder="Input name">'
  +'</td>'
  +'<td>'
  +'<input placeholder="Input action">'
  +'</td>'
  +'<td>'
  +'<input placeholder="Input action name">'
  +'</td>'
  +'<td>'
  +'<input type="button" id="btn_send" class="btn btn-success" onclick="javascript:sendDataToSystem()" value="send"></input>'
  +'</td>'
  );
  table.remove('#tableinput');
}

function encodeQueryData(data) {
  const ret = [];
  for (let d in data)
  {
    ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
}

function getData(){
  //alert("Yuhu");
  $.ajax({
    //url: "?get=data&format=json",
    url: host + "/get",
    context: document.body
  }).done(function(data) {
    // Manage header
    $('view').empty();
    $('view').append('<h1 style="font-size:50px;"><marquee>User view</marquee></h1></p>');
    let table = $('#table');
    table.remove();
    table = $('<table>');
    table.addClass("table");
    table.addClass("table-striped");
    table.addClass("table-hover");
    table.attr('id', 'table');
    table.append('<thead class="thead-dark">'
      +'<tr>'
      +'<th><div class="col-3 col-sm-1">#</div></th>'
      +'<th><div class="col-3 col-sm-1">Vorname</div></th>'
      +'<th><div class="col-3 col-sm-1">Nachname</div></th>'
      +'<th><div class="col-3 col-sm">Fähigkeit</div></th>'
      +'<th></th>'
      +'</tr>'
      +'</thead>'
    );
    table.append('<tr>');
    // Does not work with separated cmds, only inline like above :/
    table.append('<tbody>');
    let tabledata = '';
    for(let i=0; i<data.length; i++){
      // Does not work with separated cmds, only inline like above :/
      tabledata += '<tr>';
      //tabledata += '<td>'+i+' ('+data[i].id+')</td>';
      tabledata += '<td>'+i+'</td>';
      tabledata += '<td>'+data[i].firstname+'</td>';
      tabledata += '<td>'+data[i].lastname+'</td>';
      tabledata += '<td>'+data[i].ability+'</td>';
      tabledata += '</tr>';
    }
    table.append(tabledata);

    table.append('<td><input style="max-width:200px;" placeholder="Input vorname" id="input_vorname"></input></td>');
    table.append('<td><input style="max-width:200px;" placeholder="Input nachname" id="input_nachname"></input></td>');
    table.append(
      '<td>'
      +'<input type="button" id="btn_send" class="btn btn-success" onclick="javascript:sendDataToSystem()" value="send"></input>'
      +'</td>'
    );

    table.append('</tbody>');
    table.append('</table>');
    $('body').append('<p></p>');
    $('body').append(table);
  });
}

function getAllData(){
  let params = {
    "pw": "secret",
    "username": "admin",
  };
  $.ajax({
    url: host + "/get",
    context: document.body,
    data: params,
  }).done(function(data) {
    // Manage header
    $('view').empty();
    $('view').append('<h1 style="font-size:50px;"><marquee>Admin view</marquee></h1></p>');
    let table = $('#table');
    table.remove();
    table = $('<table>');
    table.addClass("table");
    table.addClass("table-striped");
    table.addClass("table-hover");
    table.attr('id', 'table');
    table.append('<thead class="thead-dark">'
        +'<tr>'
        +'<th><div class="col-3 col-sm-1">#</div></th>'
        +'<th><div class="col-3 col-sm-1">Vorname</div></th>'
        +'<th><div class="col-3 col-sm-1">Nachname</div></th>'
        +'<th><div class="col-3 col-sm-1">Emailadresse</div></th>'
        +'<th><div class="col-3 col-sm-1">Passwort</div></th>'
        +'<th><div class="col-3 col-sm-1">Fähigkeit</div></th>'
        +'<th><div class="col-3 col-sm-1">Is Admin</div></th>'
        +'<th><div class="col-3 col-sm-1">Geburtstag</div></th>'
        +'<th></th>'
        +'</tr>'
        +'</thead>'
    );
    table.append('<tr>');
    // Does not work with separated cmds, only inline like above :/
    table.append('<tbody>');
    let tabledata = '';
    for(let i=0; i<data.length; i++){
      // Does not work with separated cmds, only inline like above :/
      tabledata += '<tr>';
      //tabledata += '<td>'+i+' ('+data[i].id+')</td>';
      tabledata += '<td>'+i+'</td>';
      tabledata += '<td>'+data[i].firstname+'</td>';
      tabledata += '<td>'+data[i].lastname+'</td>';
      tabledata += '<td>'+data[i].email+'</td>';
      tabledata += '<td>'+data[i].password+'</td>';
      tabledata += '<td>'+data[i].ability+'</td>';
      if(data[i].admin){
        tabledata += '<td>Yes</td>';
      } else {
        tabledata += '<td>No</td>';
      }
      tabledata += '<td>'+data[i].birthdate+'</td>';
      tabledata += '</tr>';
    }
    table.append(tabledata);

    table.append('<td><input style="max-width:200px;" placeholder="Input vorname" id="input_vorname"></input></td>');
    table.append('<td><input style="max-width:200px;" placeholder="Input nachname" id="input_nachname"></input></td>');
    table.append(
        '<td>'
        +'<input type="button" id="btn_send" class="btn btn-success" onclick="javascript:sendDataToSystem()" value="send"></input>'
        +'</td>'
    );

    table.append('</tbody>');
    table.append('</table>');
    $('body').append('<p></p>');
    $('body').append(table);
  });
}

function newData(newData) {
  let data = {
    "pw": "secret",
    "username": "admin",
  };
  let url;
  if(newData){
    url = host + "/createNewData?";
  } else {
    url = host + "/createMoreData?";
  }
  $.ajax({
    url: url,
    context: document.body,
    type: "POST",
    data: data,
    success: function (data) {
      if(newData){
        alert("Generated new data.");
      } else {
        alert("Inserted new data.");
      }
      getData();
    },
    error: function (error) {
      alert("Status: error, message: " + error.responseText);
      console.log("Status: error, message: " + error.responseText);
    }
  });
}

      ///////////////////
      /*
      //$.post("http://localhost:8000/createMoreData/?username=admin&pw=secret", function (date){
      $.post("http://localhost:8000/createMoreData", data).done(function (response){
        alert("Inserted new data.");
      }).fail(function ( jqXHR, textStatus, errorThrown ) {
        alert('Failed');
        console.log("Status: " + textStatus + ", message: " + errorThrown);
      });
    }
       */
    function sendDataToSystem(){
      let input_name_position_value = $('#input_position').val();
      let input_name_value = $('#input_name').val();
      let input_action_value = $('#input_action').val();
      let input_action_name_value = $('#input_action_name').val();
      if(input_name_position_value && input_name_value == "" && input_action_value == "" && input_action_name_value == "")
      {}
      else {
        $.ajax({
          url: "?get=add_user"
          +"&format=json"
          +"&vorname="+input_name_value
          +"&nachname="+input_name_value,
          context: document.body
        }).done(function(data) {
          alert("Inserted "
          +input_name_value+" ",
          +"' to system.");
          getData();
        }).fail(function ( jqXHR, textStatus, errorThrown ) {
          console.log(textStatus);
          alert('Failed');
        });
      }
  }

$(function() {
    $('#table').on('editable-save.bs.table', function(e, field, row, oldValue){
        console.log("1 "+ field);
        console.log("2 "+ row[field]);
        console.log("3 "+ row.lot);
        console.log("4 "+ oldValue);
    });
  });
