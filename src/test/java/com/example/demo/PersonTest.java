package com.example.demo;

import com.example.demo.controller.PersonController;
import com.example.demo.entity.Person;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.PersonService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.assertTrue;

@AutoConfigureMockMvc
@SpringBootTest
public class PersonTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Mock
    private PersonController personController;

    @InjectMocks
    PersonService personService;

    static final String url = "http://localhost:8000";

    //static final String urlCreateData = "http://localhost:8000";

    private static final Logger log = LogManager.getLogger(PersonTest.class);

    private static List<Person> personList;

    @Test
    @DisplayName("Test Controller: instance != null")
    public void start()
            throws Exception {
        assertNotNull("", personController);
    }

    @BeforeAll
    @DisplayName("Start -> Test Service: Create new object -> List with objects is not null")
    public static void init()
            throws Exception{
        log.info("Creating new objects, some with admin");
        personList = PersonService.createNewData();
    }

    @Test
    @DisplayName("Test Service: Check objects -> Check some fields to be not admin")
    public void checkObjectFields()
            throws Exception{
        if(!personList.isEmpty()) {
            for (Person person : personList) {
                if(!person.isAdmin()){
                    assertTrue(StringUtils.isNoneBlank(
                            person.getFirstname(),
                            person.getLastname(),
                            person.getAbility(),
                            person.getPassword(),
                            person.getEmail(),
                            person.getBirthdate()
                    ));
                    break;
                }
            }
        }
    }

    @Test
    @DisplayName("Test Service: Check objects -> Check some fields to be admin")
    public void checkObjectFieldsAdmin()
            throws Exception{
        if(!personList.isEmpty()) {
            for (Person person : personList) {
                if(person.isAdmin()){
                    assertTrue(StringUtils.isNoneBlank(
                            person.getFirstname(),
                            person.getLastname(),
                            person.getAbility(),
                            person.getPassword(),
                            person.getEmail(),
                            person.getBirthdate()
                    ));
                    break;
                }
            }
        }
    }

    @Test
    @DisplayName("Test Service: Check objects -> Check some fields to be admin and password is not cleartext")
    public void checkObjectFieldsAdmin_Password_cleartext()
            throws Exception{
        boolean found = false;
        PersonService.setCARE_ABOUT_PERSONAL_DATA(true);
        List<Person> personList_Sensible = personService.getDataWithoutSensibleInfos(true, personList);
        if(!personList_Sensible.isEmpty()) {
            for (Person person : personList_Sensible) {
                if(person.isAdmin()){
                    if(StringUtils.isNoneBlank(
                            person.getFirstname(),
                            person.getLastname(),
                            person.getAbility(),
                            person.getPassword(),
                            person.getEmail(),
                            person.getBirthdate())
                            && person.getPassword().equals("***")
                    ){
                        found = true;
                        break;
                    }
                }
            }
        }
        PersonService.setCARE_ABOUT_PERSONAL_DATA(false);
        assertTrue(found);
    }

    @Test
    @DisplayName("Test Service: Check objects -> Check some fields to be admin and password is cleartext")
    public void checkObjectFieldsAdminNotSensible()
            throws Exception{
        PersonService.setCARE_ABOUT_PERSONAL_DATA(false);
        List<Person> personList_Sensible = personService.getDataWithoutSensibleInfos(true, personList);
        boolean found = false;
        if(!personList_Sensible.isEmpty()) {
            for (Person person : personList_Sensible) {
                if(StringUtils.isNoneBlank(
                        person.getFirstname(),
                        person.getLastname(),
                        person.getAbility(),
                        person.getPassword(),
                        person.getEmail(),
                        person.getBirthdate())
                        && ! person.getPassword().equals("***")
                ){
                    found = true;
                    break;
                }
            }
        }
        PersonService.setCARE_ABOUT_PERSONAL_DATA(true);
        assertTrue(found);
    }

    @Test
    @DisplayName("Test Service: List with objects is not null")
    public void getDataFromService_listIsNotNull()
            throws Exception{
        assertNotNull("List with objects of type person is not null", personList);
    }

    @Test
    @DisplayName("Test Service: List with objects is not empty")
    public void getDataFromService_listIsNotEmpty()
            throws Exception{
        assertTrue(!personList.isEmpty());
    }

    @Test
    @DisplayName("Test Controller: DB is not empty after action")
    public void getDataFromService_dbIsNotEmptyAfterAction()
            throws Exception{
        //personController.isDatabaseEmpty();
        personController.emptyDBAndcreateNewData();
        assertFalse(personRepository.findAll().isEmpty());
    }

    @Test
    @DisplayName("Test Website: Request -> Status ok")
    public void request_status_ok()
            throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    /*
    @Test
    @DisplayName("Website Request: Status ok & json object exists")
    public void request_status_ok_and_json_object_exists()
            throws Exception {
        log.info("Start -> Request: Status ok & json object exists");
        mockMvc.perform(MockMvcRequestBuilders
                        .get(url)
                        .accept(MediaType.APPLICATION_JSON))
                //.andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstname").exists());
    }
    */
    @Test
    @DisplayName("Test Website: Request -> Status ok & json object exists & json object gt null")
    public void request_status_ok_and_json_object_gt_null()
            throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(url+"/get")
                        .accept(MediaType.APPLICATION_JSON))
                //.andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").isArray());
    }
}
